using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace HealthyBMI.Models
{
	public class Blog {
		
		public int BlogID { get; set;}
		public int weight {get; set;}
		public int height { get; set;}
		public float finalbmi {get; set;}
		
		[DataType(DataType.Date)]
		public string ReportDate {get; set;}
		
		
		public string NewsUserId {get; set;}
		public NewsUser postUser {get; set;}
		
		
	}
	
	public class NewsUser : IdentityUser{
		public string FirstName { get; set;}
		public string LastName { get; set;}
	}

}