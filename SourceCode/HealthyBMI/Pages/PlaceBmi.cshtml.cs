using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;


namespace HealthyBMI.Pages
{
    public class PlaceBmiModel : PageModel
    {
		private readonly HealthyBMI.Data.HealthyBMIContext _context;
        public PlaceBmiModel(HealthyBMI.Data.HealthyBMIContext context)
        {
            _context = context;
        }
			public  String weight;
			public  String height;
			public  String finalbmi;
		
		public async Task<IActionResult> OnPostAsync()
		{
				
				weight = Request.Form["weight"];
			    height = Request.Form["height"];
				
			  height =   "" + (float.Parse(height)/100);
			  finalbmi= "" + (float.Parse(weight)/((float.Parse(height))*(float.Parse(height))));
			  
			
			  return Page();
				
		}
    }
}