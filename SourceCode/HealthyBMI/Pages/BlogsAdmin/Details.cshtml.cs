using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using HealthyBMI.Data;
using HealthyBMI.Models;

namespace HealthyBMI.Pages.BlogsAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly HealthyBMI.Data.HealthyBMIContext _context;

        public DetailsModel(HealthyBMI.Data.HealthyBMIContext context)
        {
            _context = context;
        }

        public Blog Blog { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Blog = await _context.Blogs
                .Include(b => b.postUser).FirstOrDefaultAsync(m => m.BlogID == id);

            if (Blog == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
