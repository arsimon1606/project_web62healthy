using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using HealthyBMI.Data;
using HealthyBMI.Models;

namespace HealthyBMI.Pages.BlogsAdmin
{
    public class IndexModel : PageModel
    {
        private readonly HealthyBMI.Data.HealthyBMIContext _context;

        public IndexModel(HealthyBMI.Data.HealthyBMIContext context)
        {
            _context = context;
        }

        public IList<Blog> Blog { get;set; }

        public async Task OnGetAsync()
        {
            Blog = await _context.Blogs
                .Include(b => b.postUser).ToListAsync();
        }
    }
}
