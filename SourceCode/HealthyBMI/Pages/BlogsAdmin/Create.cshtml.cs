using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using HealthyBMI.Data;
using HealthyBMI.Models;

namespace HealthyBMI.Pages.BlogsAdmin
{
    public class CreateModel : PageModel
    {
        private readonly HealthyBMI.Data.HealthyBMIContext _context;

        public CreateModel(HealthyBMI.Data.HealthyBMIContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Blog Blog { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Blogs.Add(Blog);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}