using HealthyBMI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace HealthyBMI.Data
{
	public class HealthyBMIContext : IdentityDbContext<NewsUser>
	{
		public DbSet<Blog> Blogs {get; set;}
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=Blog.db");
		}
	}
}